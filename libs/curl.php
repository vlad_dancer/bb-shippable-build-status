<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Hranicka\BuildStatus;

class CurlFactory
{

	/** @var array */
	private $options = [];

	/**
	 * @param array $options
	 */
	public function __construct(array $options)
	{
		$this->options = $options;
	}

	/**
	 * @param string $url
	 * @return Curl
	 */
	public function create($url)
	{
		$curl = new Curl($url);

		foreach ($this->options as $option) {
			$curl->addOption($option[0], $option[1]);
		}

		return $curl;
	}

}

class Curl
{

	const METHOD_POST = 'POST';
	const METHOD_GET = 'GET';
	const METHOD_PUT = 'PUT';

	/** @var string */
	private $url;

	/** @var string */
	private $method;

	/** @var array */
	private $parameters = [];

	/** @var array */
	private $headers = [];

	/** @var array */
	private $options = [];

	public function __construct($url)
	{
		$this->url = $url;
	}

	/**
	 * @param mixed $name
	 * @param mixed $value
	 * @return $this
	 */
	public function addParameter($name, $value)
	{
		$this->parameters[$name] = $value;
		return $this;
	}

	/**
	 * @param array $parameters
	 * @return $this
	 */
	public function appendParameters(array $parameters)
	{
		foreach ($parameters as $name => $value) {
			$this->addParameter($name, $value);
		}

		return $this;
	}

	/**
	 * @param $header
	 * @return $this
	 */
	public function addHeader($header)
	{
		$this->headers[] = $header;
		return $this;
	}

	/**
	 * @param int $option CURLOPT_* constant.
	 * @param $value
	 * @return $this
	 */
	public function addOption($option, $value)
	{
		$this->options[] = [$option, $value];
		return $this;
	}

	/**
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function post()
	{
		$this->method = self::METHOD_POST;
		return $this->exec();
	}

	/**
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function put()
	{
		$this->method = self::METHOD_PUT;
		return $this->exec();
	}

	/**
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function get()
	{
		$this->method = self::METHOD_GET;
		return $this->exec();
	}

	/**
	 * @return mixed
	 * @throws RequestFailureException
	 */
	private function exec()
	{
		$url = $this->url;

		$options = $this->options;
		$options[] = [CURLOPT_RETURNTRANSFER, TRUE];

		if ($this->method === self::METHOD_POST) {
			$options[] = [CURLOPT_POST, TRUE];
		} elseif ($this->method === self::METHOD_PUT) {
			$option[] = [CURLOPT_CUSTOMREQUEST, 'PUT'];
		}

		if ($this->parameters) {
			if ($this->method === self::METHOD_GET) {
				$url .= '?' . http_build_query($this->parameters);
			} else {
				$options[] = [CURLOPT_POSTFIELDS, http_build_query($this->parameters)];
			}
		}

		if ($this->headers) {
			$options[] = [CURLOPT_HTTPHEADER, $this->headers];
		}

		$curl = curl_init($url);

		foreach ($options as $option) {
			curl_setopt($curl, $option[0], $option[1]);
		}

		$response = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$curlError = curl_error($curl);
		$curlErrorNo = curl_errno($curl);
		curl_close($curl);

		if ($status != 200) {
			throw new RequestFailureException("Call to URL '{$this->url}' failed with status '$status', response '$response', " .
				"curl_error '$curlError', curl_errno '$curlErrorNo'.");
		}

		return $response;
	}

}

class RequestFailureException extends \Exception
{

}
