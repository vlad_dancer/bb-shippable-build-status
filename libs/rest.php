<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Hranicka\BuildStatus;

class RestClient
{

	/** @var CurlFactory */
	private $curlFactory;

	/** @var string */
	private $accessToken;

	public function __construct(CurlFactory $curlFactory)
	{
		$this->curlFactory = $curlFactory;
	}

	/**
	 * @param string $url
	 * @param array $parameters
	 * @throws RequestFailureException
	 */
	public function authorize($url, array $parameters = [])
	{
		$json = json_decode($this->createCurl($url, $parameters)->post(), TRUE);
		$this->accessToken = $json['access_token'];
	}

	/**
	 * @param string $url
	 * @param array $parameters
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function get($url, array $parameters = [])
	{
		return $this->createCurl($url, $parameters)->get();
	}

	/**
	 * @param string $url
	 * @param array $parameters
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function post($url, array $parameters = [])
	{
		return $this->createCurl($url, $parameters)->post();
	}

	/**
	 * @param string $url
	 * @param array $parameters
	 * @return mixed
	 * @throws RequestFailureException
	 */
	public function put($url, array $parameters = [])
	{
		return $this->createCurl($url, $parameters)->put();
	}

	/**
	 * @param string $url
	 * @param array $parameters
	 * @return Curl
	 */
	private function createCurl($url, array $parameters = [])
	{
		$curl = $this->curlFactory->create($url);

		if ($this->accessToken) {
			$curl->addHeader('Authorization: Bearer ' . $this->accessToken);
		}

		$curl->appendParameters($parameters);

		return $curl;
	}

}
