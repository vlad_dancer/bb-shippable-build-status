# Build Status hook service

Only BitBucket is supported.

It's only a DEV version and its purpose is
[BitBucket Build API](https://confluence.atlassian.com/bitbucket/statuses-build-resource-779295267.html) absence in
[Shippable](https://github.com/Shippable/support/issues/2054).


## Installation (BitBucket integration)

* Create OAuth consumer in BitBucket with "Repository:write", see [BitBucket's guide](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html#OAuthonBitbucketCloud-Createaconsumer).

* Create file `config/oauth.ini` and add OAuth credentials from BitBucket.

```
#!ini

[yourAccountName]
client_id = "key"
client_secret = "secret"
```

* Deploy the app on a public server where you can call it from Shippable (see Examples).


## Examples

### [shippable.yml](http://docs.shippable.com/yml_reference/)
	
```
#!yaml

before_script:
  - curl "http://example.com/build-status/bitbucket.php?repository=$REPO_NAME&revision=$COMMIT&buildId=$SHIPPABLE_BUILD_ID&key=SHIPPABLE&name=Shippable&url=$BUILD_URL&description=&state=INPROGRESS"

after_success:
  - curl "http://example.com/build-status/bitbucket.php?repository=$REPO_NAME&revision=$COMMIT&buildId=$SHIPPABLE_BUILD_ID&key=SHIPPABLE&name=Shippable&url=$BUILD_URL&description=&state=SUCCESSFUL"

after_failure:
  - curl "http://example.com/build-status/bitbucket.php?repository=$REPO_NAME&revision=$COMMIT&buildId=$SHIPPABLE_BUILD_ID&key=SHIPPABLE&name=Shippable&url=$BUILD_URL&description=&state=FAILED"

```
